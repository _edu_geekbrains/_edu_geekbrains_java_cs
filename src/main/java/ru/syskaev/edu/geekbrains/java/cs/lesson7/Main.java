package ru.syskaev.edu.geekbrains.java.cs.lesson7;

import ru.syskaev.edu.geekbrains.java.cs.lesson3.Person;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static CustomHashMap hashMap;

    public static void main(String[] args) {
        startCli();
    }

    private static void startCli() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nBASE MODE\n")
                    .append("Choose:\n")
                    .append("1 Create HasMap\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        int capacity = getIntInput("Enter minimal capacity >= 3 and <= 1000", 3, 1000);
                        hashMap = new CustomHashMap(capacity);
                        startHashMapMode();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static void startHashMapMode() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nHASH MAP MODE\n")
                    .append("Choose:\n")
                    .append("1 Show all values\n")
                    .append("2 Insert\n")
                    .append("3 Insert few random items\n")
                    .append("4 Find by key\n")
                    .append("5 Delete by key\n")
                    .append("6 Check capacity factor\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        hashMap.showAll();
                        break;
                    case 2:
                        int key = getIntInput("Enter key > 0", 1, Integer.MAX_VALUE);
                        hashMap.insert(key, null);
                        System.out.println("Success");
                        break;
                    case 3:
                        int number = getIntInput("Enter number > 0", 1, Integer.MAX_VALUE);
                        while(number-- != 0)
                            hashMap.insert();
                        System.out.println("Success");
                        break;
                    case 4:
                        key = getIntInput("Enter key > 0", 1, Integer.MAX_VALUE);
                        Person person = hashMap.find(key);
                        if(person == null)
                            System.out.println("Person by key not found");
                        else
                            System.out.println(person);
                        break;
                    case 5:
                        key = getIntInput("Enter key > 0", 1, Integer.MAX_VALUE);
                        boolean result = hashMap.delete(key);
                        if(result)
                            System.out.println("Person deleted");
                        else
                            System.out.println("Person by key not found");
                        break;
                    case 6:
                        hashMap.checkCapacity();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static int getIntInput(String msg, int l, int r) {
        while(true) {
            System.out.println(msg);
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                if(choice <= r && choice >= l)
                    return choice;
                else
                    System.out.println("Invalid input");
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

}

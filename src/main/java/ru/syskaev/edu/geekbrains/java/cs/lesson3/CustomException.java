package ru.syskaev.edu.geekbrains.java.cs.lesson3;

public class CustomException extends Exception {

    public CustomException(String msg) {
        super(msg);
    }

}

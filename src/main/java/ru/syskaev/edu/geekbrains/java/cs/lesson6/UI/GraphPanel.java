package ru.syskaev.edu.geekbrains.java.cs.lesson6.UI;

import ru.syskaev.edu.geekbrains.java.cs.lesson6.Graph;

import javax.swing.*;
import java.awt.*;

import static ru.syskaev.edu.geekbrains.java.cs.lesson6.Graph.VERTEX_NUMBER;
import static ru.syskaev.edu.geekbrains.java.cs.lesson6.UI.UIConsts.*;

public class GraphPanel extends JPanel {

    private static final int vertexCoords[][] =
            {{1, 2}, {1, 3}, {2, 1}, {2, 5}, {3, 1}, {4, 6}, {5, 6}, {6, 4}, {6, 5}, {5, 2}};

    private static int gridStepX, gridStepY, vertexR = 30;
    private Graph graph;


    public void setGraph(Graph graph) { this.graph = graph; }

    public GraphPanel() {
        setBackground(GRAPH_PANEL_COLOR);
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 3));
        setPreferredSize(GRAPH_PANEL_SIZE);
        gridStepX = getWidth() / 7;
        gridStepY = getHeight() / 7;
        (new paintThread()).start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(graph == null || !graph.isReady()) {
            gridStepX = getWidth() / 7;
            gridStepY = getHeight() / 7;
            return;
        }
        drawEdges(g);
        drawVertex(g);
    }

    private void drawEdges(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));
        for(int i = 0; i != VERTEX_NUMBER; i++)
            for(int j = 0; j != VERTEX_NUMBER; j++)
                if(graph.hasEdge(i, j) != 0) {
                    if(graph.hasEdge(i, j) == 2)
                        g2.setColor(Color.CYAN);
                    else
                        g2.setColor(Color.GRAY);
                    g2.drawLine(vertexCoords[i][0] * gridStepX, vertexCoords[i][1] * gridStepY,
                            vertexCoords[j][0] * gridStepX, vertexCoords[j][1] * gridStepY);
                }
    }

    private void drawVertex(Graphics g) {
        String strForVertex;
        for(int i = 0; i != VERTEX_NUMBER; i++) {
            if (i == graph.getStartVertexIndex()) {
                g.setColor(Color.BLUE);
                strForVertex = "S";
            }
            else if (i == graph.getTargetVertexIndex() && graph.getVisitIndex(i) == -1) {
                g.setColor(Color.BLUE);
                strForVertex = "T";
            }
            else if (i == graph.getTargetVertexIndex()) {
                g.setColor(DARK_GREEN_COLOR);
                strForVertex = graph.getVisitIndex(i) + "";
            }
            else if(graph.getVisitIndex(i) != -1) {
                g.setColor(Color.GREEN);
                strForVertex = graph.getVisitIndex(i) + "";
            }
            else {
                g.setColor(Color.RED);
                strForVertex = "";
            }
            g.fillOval(gridStepX * vertexCoords[i][0] - vertexR / 2, gridStepY * vertexCoords[i][1] - vertexR / 2,
                    vertexR, vertexR);
            Font font = new Font("", Font.PLAIN, 24);
            g.setFont(font);
            g.setColor(Color.BLACK);
            g.drawString(strForVertex, gridStepX * vertexCoords[i][0] - vertexR,
                    gridStepY * vertexCoords[i][1] - vertexR / 2);
        }
    }

    private class paintThread extends Thread {

        @Override
        public synchronized void run() {
            while(true) {
                repaint();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}

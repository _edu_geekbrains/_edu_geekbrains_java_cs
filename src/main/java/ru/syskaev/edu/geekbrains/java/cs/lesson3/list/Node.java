package ru.syskaev.edu.geekbrains.java.cs.lesson3.list;

public class Node<T> {

    private T data;
    private Node next = null;
    private Node prev = null;

    public Node(T data) { this.data = data; }

    public Node getNext() { return next; }
    public Node getPrev() { return prev; }
    public void setNext(Node next) { this.next = next; }
    public void setPrev(Node prev) { this.prev = prev; }
    public T getData() { return data; }

    public void print() {
        System.out.println(data.toString());
    }

}

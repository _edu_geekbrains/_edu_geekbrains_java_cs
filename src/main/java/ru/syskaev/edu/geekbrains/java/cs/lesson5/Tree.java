package ru.syskaev.edu.geekbrains.java.cs.lesson5;

import java.util.Random;

public class Tree {

    private static final int MAX_DEEP = 6;
    private static final int MAX_VALUE_ABS = 99;

    private Node root = null;
    private Random random = new Random();
    private boolean balanceFlag = true;

    public boolean addRandomVal() {
        return add(root, 1, getRandomVal());
    }

    private int getRandomVal() {
        return random.nextInt(2 * MAX_VALUE_ABS + 1) - MAX_VALUE_ABS;
    }

    private boolean add(Node currentNode, int currentDeep, int value) {
        if(currentNode == null) {
            root = new Node(null, value);
            return true;
        }
        else if(value >= currentNode.value) {
            if(currentNode.right == null) {
                if(currentDeep <= 5) {
                    currentNode.right = new Node(currentNode, value);
                    return true;
                }
                else
                    return false;
            }
            else
                return add(currentNode.right, currentDeep + 1, value);
        }
        else /*if(value < currentNode.value)*/ {
            if(currentNode.left == null) {
                if(currentDeep <= 5) {
                    currentNode.left = new Node(currentNode, value);
                    return true;
                }
                else
                    return false;
            }
            else
                return add(currentNode.left, currentDeep + 1, value);        }
    }

    public void displayTree() {
        (new DisplayTask()).display();
    }

    public boolean checkBalance() {
        calcAllDepths(root);
        return balanceFlag;
    }

    private int calcAllDepths(Node node) {
        int deepRight = (node.right == null) ? 0 : calcAllDepths(node.right);
        int deepLeft = (node.left == null) ? 0 : calcAllDepths(node.left);
        if(Math.abs(deepLeft-deepRight) > 1)
            balanceFlag = false;
        return Math.max(deepLeft, deepRight) + 1;
    }

    private class Node {

        private int value;
        private Node parent;
        private Node left = null;
        private Node right = null;

        public Node(Node parent, int value) {
            this.parent = parent;
            this.value = value;
        }
    }

    private class DisplayTask {

        private final int ARRAY_COL = (int)(Math.pow(2, MAX_DEEP));
        private final int ARRAY_ROW = MAX_DEEP * 2;
        private final String ONLY_RIGHT = "  \\";
        private final String ONLY_LEFT = "/  ";
        private final String LEFT_AND_RIGHT = "/ \\";
        private final String VOID = "   ";

        private String outArray[][] = new String[ARRAY_ROW][ARRAY_COL];

        private void display() {
            prepareArray(root, 0, ARRAY_COL / 2 - 1, ARRAY_COL / 2);
            for (int i = 0; i != ARRAY_ROW; i+=2) {
                System.out.print("lvl" + (i/2 + 1) + ":  ");
                for (int j = 0; j != ARRAY_COL; j++)
                    if (outArray[i][j] == null)
                        System.out.print(VOID);
                    else
                        System.out.print(outArray[i][j]);
                System.out.println();
                System.out.print("       ");
                for (int j = 0; j != ARRAY_COL; j++)
                    if (outArray[i+1][j] == null)
                        System.out.print(VOID);
                    else
                        System.out.print(outArray[i+1][j]);
                System.out.println();
                System.out.println();
            }
            System.out.println("\n\n\n");
        }

        private void prepareArray(Node node, int currentDeep, int currentPosition, int currentShift) {
            outArray[currentDeep * 2][currentPosition] = String.format("%3d", node.value);
            if(node.right != null && node.left != null)
                outArray[currentDeep * 2 + 1][currentPosition] = LEFT_AND_RIGHT;
            else if(node.right != null)
                outArray[currentDeep * 2 + 1][currentPosition] = ONLY_RIGHT;
            else if(node.left != null)
                outArray[currentDeep * 2 + 1][currentPosition] = ONLY_LEFT;
            if(node.right != null)
                prepareArray(node.right, currentDeep + 1,
                        currentPosition + currentShift/2, currentShift/2);
            if(node.left != null)
                prepareArray(node.left, currentDeep + 1,
                        currentPosition - currentShift/2, currentShift/2);

        }


    }

}

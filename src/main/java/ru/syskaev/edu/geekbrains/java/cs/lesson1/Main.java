package ru.syskaev.edu.geekbrains.java.cs.lesson1;

public class Main {

    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        CustomArray defaultArray = new CustomArray(50);
        defaultArray.randomFill();
        defaultArray.print();
        defaultArray.copy().sort(CustomArray.SortType.BUBBLE, false, true);
        defaultArray.copy().sort(CustomArray.SortType.SELECT, false, true);
        defaultArray.copy().sort(CustomArray.SortType.INSERT, false, true);
        defaultArray.copy().sort(CustomArray.SortType.QUICK, false, true);
        defaultArray.copy().sort(CustomArray.SortType.MERGE, false, true);
        System.out.println();
    }

    private static void test2() {
        CustomArray defaultArray = new CustomArray();
        defaultArray.randomFill();
        defaultArray.copy().sort(CustomArray.SortType.BUBBLE, true, false);
        defaultArray.copy().sort(CustomArray.SortType.SELECT, true, false);
        defaultArray.copy().sort(CustomArray.SortType.INSERT, true, false);
        defaultArray.copy().sort(CustomArray.SortType.QUICK, true, false);
        defaultArray.copy().sort(CustomArray.SortType.MERGE, true, false);
        System.out.println();
    }

}

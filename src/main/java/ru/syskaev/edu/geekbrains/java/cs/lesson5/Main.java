package ru.syskaev.edu.geekbrains.java.cs.lesson5;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Trees trees;

    public static void main(String[] args) {
        startCli();
    }

    private static void startCli() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nBASE MODE\n")
                    .append("Choose:\n")
                    .append("1 Create trees\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        int number = getIntInput("Enter number of trees <= 1000000", 1, 1000000);
                        trees = new Trees(number);
                        startTreesMode();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static void startTreesMode() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nTREES MODE\n")
                    .append("Choose:\n")
                    .append("1 Show tree by index\n")
                    .append("2 Show all trees\n")
                    .append("3 Check balance\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        int index = getIntInput("Enter index", 1, trees.getListSize());
                        trees.showTree(index);
                        break;
                    case 2:
                        if(trees.getListSize() <= 100)
                            trees.showTrees();
                        else
                            System.out.println("Too many trees");
                        break;
                    case 3:
                        trees.checkBalance();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static int getIntInput(String msg, int l, int r) {
        while(true) {
            System.out.println(msg);
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                if(choice <= r && choice >= l)
                    return choice;
                else
                    System.out.println("Invalid input");
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

}

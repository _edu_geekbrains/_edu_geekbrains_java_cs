package ru.syskaev.edu.geekbrains.java.cs.lesson4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Rucksack {

    private int size, itemsNumber;
    private List<Item> items;
    private Random random = new Random();

    public Rucksack(int size, int itemsNumber) {
        this.size = size;
        this.itemsNumber = itemsNumber;
        items = new ArrayList<>(itemsNumber);
        createItems();
        displayItems();
    }

    private void createItems() {
        int itemMinSize = size / 10;
        int itemMaxSize = size / 3;
        int itemMinValuePerSize = 1;
        int itemMaxValuePerSize = 10;
        for(int i = 0; i != itemsNumber; i++) {
            int tempSize = getIntBetween(itemMinSize, itemMaxSize);
            items.add(new Item(tempSize, tempSize * getIntBetween(itemMinValuePerSize, itemMaxValuePerSize)));
        }
    }

    private void displayItems() {
        System.out.println("Rucksack and items created:");
        System.out.println("Rucksack size = " + size);
        for(int i = 0; i != itemsNumber; i++)
            System.out.println(String.
                    format("Item %d: size = %d, value = %d", i+1, items.get(i).size, items.get(i).value));
    }

    public void solveRucksackTask() {
        RucksackTask rucksackTask = new RucksackTask();
        rucksackTask.solve();
        rucksackTask.showResult();
    }

    private int getIntBetween(int l, int r) {
        return random.nextInt(r - l + 1) + l;
    }

    private class Item{

        private int size;
        private int value;
        private boolean inRucksack = false;
        private boolean inGoodRucksack = false;

        private Item(int size, int value) {
            this.size = size;
            this.value = value;
        }
    }

    private class RucksackTask{

        int currentItemsSize = 0;
        int currentValue = 0;
        int currentMaxValue = 0;
        int currentSizeForMaxValue = 0;
        int currentFirstIndex = 0;
        int counter = 0;

        private void solve() {
            counter++;
            if(currentMaxValue < currentValue) {
                currentMaxValue = currentValue;
                currentSizeForMaxValue = currentItemsSize;
                for(Item item : items)
                    item.inGoodRucksack = item.inRucksack;
            }
            if(currentFirstIndex == itemsNumber)
                return;
            for(int i = currentFirstIndex; i != itemsNumber; i++) {
                Item item = items.get(i);
                if(item.inRucksack)
                    continue;
                else if(currentItemsSize + item.size <= size){
                    currentItemsSize += item.size;
                    currentValue += item.value;
                    currentFirstIndex = i + 1;
                    item.inRucksack = true;
                    solve();
                    currentItemsSize -= item.size;
                    currentValue -= item.value;
                    item.inRucksack = false;
                }
            }
        }

        private void showResult() {
            System.out.println("\nTask solved:");
            for(int i = 0; i != itemsNumber; i++)
                if(items.get(i).inGoodRucksack)
                    System.out.println(String.
                            format("Item %d: size = %d, value = %d", i+1, items.get(i).size, items.get(i).value));
            System.out.println("Total size = " + currentSizeForMaxValue);
            System.out.println("Total value = " + currentMaxValue);
            //System.out.println("Counter = " + counter);
        }

    }

}

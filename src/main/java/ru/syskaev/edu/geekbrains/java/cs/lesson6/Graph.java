package ru.syskaev.edu.geekbrains.java.cs.lesson6;

import ru.syskaev.edu.geekbrains.java.cs.lesson6.UI.LogPanel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Graph{

    public static final int VERTEX_NUMBER = 10;
    public static final int MAX_EDGE_NUMBER = ((VERTEX_NUMBER - 1) * VERTEX_NUMBER) / 2;

    private List<Vertex> list = new ArrayList<>(VERTEX_NUMBER);
    private List<Vertex> bufferList = new LinkedList<>();
    private List<Vertex> tempBufferList = new LinkedList<>();
    private List<List<Integer>> matrix = new ArrayList<>(VERTEX_NUMBER);
    private int currentEdgeNumber = 0;
    private Random random = new Random();
    private final LogPanel logPanel;
    private boolean ready = false;
    private int startVertexIndex = -1, targetVertexIndex = -1;

    public synchronized int getCurrentEdgeNumber() { return currentEdgeNumber; }
    public synchronized boolean isReady() { return ready; }
    public synchronized void setReady(boolean ready) { this.ready = ready; }
    public synchronized int getStartVertexIndex() { return startVertexIndex; }
    public synchronized int getTargetVertexIndex() { return targetVertexIndex; }
    public synchronized int getVisitIndex(int vertexIndex) { return list.get(vertexIndex).getVisitIndex(); }

    public Graph(LogPanel logPanel) {
        this.logPanel = logPanel;
        for(int i = 0; i < VERTEX_NUMBER; i++) {
            Vertex vertex = new Vertex(i);
            list.add(vertex);
            List<Integer> tempList = new ArrayList<>(VERTEX_NUMBER);
            for(int j = 0; j != 10; j++)
                tempList.add(0);
            matrix.add(tempList);
        }
        logPanel.appendLog("Vertex created");
        setReady(true);
    }

    public void addRandomEdge() {
        if(currentEdgeNumber == MAX_EDGE_NUMBER)
            return;
        while(true) {
            int src = random.nextInt(VERTEX_NUMBER);
            int tgt = random.nextInt(VERTEX_NUMBER);
            if(src == tgt)
                continue;
            else if(matrix.get(src).get(tgt) != 0)
                continue;
            else {
                matrix.get(src).set(tgt, 1);
                matrix.get(tgt).set(src, 1);
                currentEdgeNumber++;
                sleep(150);
                return;
            }
        }
    }

    public void displayMatrix() {
        for(List<Integer> list : matrix) {
            for(Integer val : list)
                System.out.print(val);
            System.out.println();
        }
    }

    public void startBFS() {
        startVertexIndex = random.nextInt(VERTEX_NUMBER);
        while(startVertexIndex == targetVertexIndex || targetVertexIndex == -1)
            targetVertexIndex = random.nextInt(VERTEX_NUMBER);
        sleep(150);
        logPanel.appendLog("Start vertex selected: " + startVertexIndex);
        sleep(150);
        logPanel.appendLog("Target vertex selected: " + targetVertexIndex);
        sleep(150);
        logPanel.appendLog("Start BFS algorithm");
        bufferList.add(list.get(startVertexIndex));
        list.get(startVertexIndex).setVisitIndex(0);
        bfs(0);
    }

    private void bfs(int deep) {
        sleep(1000);
        logPanel.appendLog("Step: " + (deep + 1));
        boolean stopFlag = true;
        boolean pathFoundFlag = false;
        for(Vertex vertex : bufferList){
            if(vertex.getVisitIndex() == deep) {
                for (Vertex otherVertex : list)
                    if (otherVertex.getVisitIndex() == -1 && hasEdge(vertex, otherVertex)) {
                        matrix.get(vertex.getIndex()).set(otherVertex.getIndex(), 2);
                        matrix.get(otherVertex.getIndex()).set(vertex.getIndex(), 2);
                        tempBufferList.add(otherVertex);
                        otherVertex.setVisitIndex(deep + 1);
                        if(otherVertex.getIndex() == targetVertexIndex)
                            pathFoundFlag = true;
                        stopFlag = false;
                    }
            }
        }
        bufferList = tempBufferList;
        tempBufferList = new LinkedList<>();
        if(stopFlag || pathFoundFlag) {
            sleep(150);
            logPanel.appendLog("Algorithm finished");
            sleep(150);
            if(stopFlag)
                logPanel.appendLog("Path not found\n");
            else
                logPanel.appendLog("Path found: " + (deep + 1) + " edges\n");
            return;
        }
        bfs(deep+1);
    }

    private boolean hasEdge(Vertex src, Vertex tgt) {
        return matrix.get(src.getIndex()).get(tgt.getIndex()) != 0;
    }

    public synchronized int hasEdge(int srcVertexIndex, int tgtVertexIndex) {
        return matrix.get(srcVertexIndex).get(tgtVertexIndex);
    }

    private void sleep(int milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Vertex {

        int index;
        int visitIndex = -1;

        public int getIndex() { return index; }
        public void setVisitIndex(int index) { this.visitIndex = index; }
        public int getVisitIndex() { return visitIndex; }

        public Vertex(int index) {
            this.index = index;
        }
    }

}

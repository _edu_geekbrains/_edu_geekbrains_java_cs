package ru.syskaev.edu.geekbrains.java.cs.lesson6.UI;

import java.awt.*;

class UIConsts {

    private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    private static final double WINDOW_WIDTH_RELATIVE_TO_SCREEN_WIDTH = 0.65;
    private static final double WINDOW_HEIGHT_RELATIVE_TO_SCREEN_HEIGHT = 0.6;
    private static final double GRAPH_PANEL_WIDTH_RELATIVE_TO_WINDOW_WIDTH = 0.65;
    private static final double BUTTON_HEIGHT_RELATIVE_TO_LOG_PANEL_HEIGHT = 0.1;
    private static final int HEURISTIC_WIDTH_SHIFT = 6; //only Win7 test

    public static final Dimension WINDOW_SIZE = new Dimension(
            (int) (SCREEN_SIZE.width * WINDOW_WIDTH_RELATIVE_TO_SCREEN_WIDTH),
            (int) (SCREEN_SIZE.height * WINDOW_HEIGHT_RELATIVE_TO_SCREEN_HEIGHT));
    public static final Dimension GRAPH_PANEL_SIZE = new Dimension(
            (int) (WINDOW_SIZE.width * GRAPH_PANEL_WIDTH_RELATIVE_TO_WINDOW_WIDTH),
            Integer.MAX_VALUE);
    private static final int LOG_PANEL_WIDTH = WINDOW_SIZE.width - GRAPH_PANEL_SIZE.width - HEURISTIC_WIDTH_SHIFT;
    private static final int LOG_PANEL_HEIGHT = WINDOW_SIZE.height;
    public static final Dimension LOG_PANEL_SIZE = new Dimension(LOG_PANEL_WIDTH, LOG_PANEL_HEIGHT);
    public static final Dimension BUTTON_SIZE = new Dimension(Integer.MAX_VALUE,
            (int) (LOG_PANEL_HEIGHT * BUTTON_HEIGHT_RELATIVE_TO_LOG_PANEL_HEIGHT));
    public static final Dimension LOG_AREA_SIZE = new Dimension(LOG_PANEL_WIDTH,
            LOG_PANEL_SIZE.height - BUTTON_SIZE.height);

    static final Color GRAPH_PANEL_COLOR = new Color(210, 210, 250);
    static final Color LOG_FONT_COLOR = new Color(30, 160, 30);
    static final Color DARK_GREEN_COLOR = new Color(0, 100, 0);

}

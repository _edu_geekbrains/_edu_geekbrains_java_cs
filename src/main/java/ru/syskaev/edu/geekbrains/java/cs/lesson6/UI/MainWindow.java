package ru.syskaev.edu.geekbrains.java.cs.lesson6.UI;

import javax.swing.*;
import java.awt.*;

import static ru.syskaev.edu.geekbrains.java.cs.lesson6.UI.UIConsts.*;

public class MainWindow extends JFrame {

    public MainWindow() {
        setTitle("BFS Visualization");
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WINDOW_SIZE);
        setLocationRelativeTo(null);

        LogPanel logPanel = new LogPanel();
        add(logPanel, BorderLayout.EAST);
        GraphPanel graphPanel = new GraphPanel();
        add(graphPanel, BorderLayout.WEST);
        logPanel.setGraphPanel(graphPanel);
    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson2;

public class PriorityQueue extends Queue {

    public PriorityQueue(int size) {
        super(size);
    }

    @Override
    public void push(int val) throws CustomException {
        if(!isFull()) {
            back = prevIndex(back);
            baseArray[back] = val;
            currentSize++;
            if(currentSize == 1)
                return;
            int currentIndex = nextIndex(back);
            while(true) {
                if (baseArray[currentIndex] > val) {
                    baseArray[prevIndex(currentIndex)] = baseArray[currentIndex];
                    baseArray[currentIndex] = val;
                } else
                    break;
                if(currentIndex == front)
                    break;
                currentIndex = nextIndex(currentIndex);
            }
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is full");
    }

}

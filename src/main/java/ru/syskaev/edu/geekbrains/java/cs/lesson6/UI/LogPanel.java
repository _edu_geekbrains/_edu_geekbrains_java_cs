package ru.syskaev.edu.geekbrains.java.cs.lesson6.UI;

import ru.syskaev.edu.geekbrains.java.cs.lesson6.Graph;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

import static ru.syskaev.edu.geekbrains.java.cs.lesson6.UI.UIConsts.*;

public class LogPanel extends JPanel {

    private static final String STR_NEW = "New demonstration";
    private static final String STR_WAIT = "Wait, please";

    private final StringBuilder logText = new StringBuilder();
    private JButton button;
    private JTextPane logArea;
    private JScrollPane scrollPane;
    private GraphPanel graphPanel;
    private BFSTread bfsTread;
    private Random random = new Random();

    public void setGraphPanel(GraphPanel graphPanel) { this.graphPanel = graphPanel; }

    public LogPanel() {
        setBackground(Color.BLACK);
        setPreferredSize(LOG_PANEL_SIZE);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        createButton();
        add(button);
        createLogArea();
        add(scrollPane);
    }

    private void createButton() {
        button = new JButton();
        button.setPreferredSize(BUTTON_SIZE);
        button.setMaximumSize(BUTTON_SIZE);
        button.setMinimumSize(BUTTON_SIZE);
        button.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 3));
        button.setSelected(false);
        button.setFocusable(false);
        button.setText(STR_NEW);
        button.addActionListener(e -> clickButton());
    }

    private void clickButton() {
        bfsTread = new BFSTread(this);
        bfsTread.start();
    }

    private class BFSTread extends Thread {

        LogPanel logPanel;

        public BFSTread(LogPanel logPanel) {
            this.logPanel = logPanel;
        }

        @Override
        public void run() {
            super.run();
            button.setText(STR_WAIT);
            button.setEnabled(false);
            Graph graph = new Graph(logPanel);
            graphPanel.setGraph(graph);
            int edgesNumber = random.nextInt(8) + 7;
            for(int i =0; i != edgesNumber; i++)
                graph.addRandomEdge();
            appendLog("Random edges created: " + edgesNumber);
            graph.startBFS();
            button.setText(STR_NEW);
            button.setEnabled(true);
        }

    }

    private void createLogArea() {
        logArea = new JTextPane();
        logArea.setEditable(false);
        logArea.setBackground(Color.BLACK);
        logArea.setForeground(LOG_FONT_COLOR);
        JScrollPane scrollPanel = new JScrollPane(logArea);
        scrollPanel.setPreferredSize(LOG_AREA_SIZE);
        scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.scrollPane = scrollPanel;
    }

    public synchronized void appendLog(Object newLine) {
        logText.append(' ').append(newLine).append("\n");
        logArea.setText(logText.toString());
        scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
    }

}

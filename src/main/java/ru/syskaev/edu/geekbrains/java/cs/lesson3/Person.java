package ru.syskaev.edu.geekbrains.java.cs.lesson3;

import java.util.Random;

public class Person {

    private static final int ASCII_SHIFT = 65;
    private static final int ALPHABET_POWER = 26;
    private static final int MIN_AGE = 18;
    private static final int AGE_DISPERSION = 10;

    private String name;
    private int age;
    private char sex;
    private Random random = new Random();

    public Person(String name, int age, char sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Person() {
        name = new StringBuilder()
                .append((char)(random.nextInt(ALPHABET_POWER) + ASCII_SHIFT))
                .append((char)(random.nextInt(ALPHABET_POWER) + ASCII_SHIFT))
                .append((char)(random.nextInt(ALPHABET_POWER) + ASCII_SHIFT))
                .append((char)(random.nextInt(ALPHABET_POWER) + ASCII_SHIFT))
                .append((char)(random.nextInt(ALPHABET_POWER) + ASCII_SHIFT))
                .toString();
        age = random.nextInt(AGE_DISPERSION) + MIN_AGE;
        sex = (random.nextBoolean() ? 'f' : 'm' );
    }

    public String getName() { return name; }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }
}

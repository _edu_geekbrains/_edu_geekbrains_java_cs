package ru.syskaev.edu.geekbrains.java.cs.lesson2;

public class BaseStructure {

    private static final int DEFAULT_MAX_SIZE = 10;

    protected int[] baseArray;
    protected int maxSize;
    protected int currentSize;

    protected BaseStructure() {
        baseArray = new int[DEFAULT_MAX_SIZE];
        maxSize = DEFAULT_MAX_SIZE;
        currentSize = 0;
    }

    protected BaseStructure(int size) {
        baseArray = new int[size];
        maxSize = size;
        currentSize = 0;
    }

    protected boolean isEmpty() {
        return currentSize == 0;
    }

    protected boolean isFull() {
        return currentSize == maxSize;
    }

    protected int nextIndex(int currentIndex) {
        return (currentIndex + 1) % maxSize;
    }

    protected int prevIndex(int currentIndex) {
        return (currentIndex + maxSize - 1) % maxSize;
    }

    protected void print() throws CustomException {
        throw new CustomException("No implemented");
    }

}

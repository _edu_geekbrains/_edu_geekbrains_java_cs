package ru.syskaev.edu.geekbrains.java.cs.lesson1;

import java.util.Random;

public class CustomArray {

    private static final int DEFAULT_SIZE = 1000000;
    private static final int DEFAULT_RESERVE = DEFAULT_SIZE / 10;
    private static final int MAX_VAL_ABS = 1000;

    enum SortType {BUBBLE, SELECT, INSERT, QUICK, MERGE}

    private static Random random = new Random();
    private int[] array;
    private int[] mergeSortArray;
    private int currentLength;
    private boolean isSorted = false;

    private int getCurrentLength() { return currentLength; }
    private int getRealLength() { return array.length; }
    private int[] getInternalArray() { return array; }

    public CustomArray() {
        array = new int[DEFAULT_SIZE + DEFAULT_RESERVE];
        currentLength = DEFAULT_SIZE;
    }

    public CustomArray(int size) {
        array = new int[size + DEFAULT_RESERVE];
        currentLength = size;
    }

    //copy constructor
    private CustomArray(CustomArray sourceArray) {
        array = new int[sourceArray.getRealLength()];
        currentLength = sourceArray.getCurrentLength();
        System.arraycopy(sourceArray.getInternalArray(), 0, array,0, currentLength);
    }

    public CustomArray copy() {
        return new CustomArray(this);
    }

    public void randomFill() {
        for (int i = 0; i < currentLength; i++)
            array[i] = random.nextInt(2 * MAX_VAL_ABS) - MAX_VAL_ABS;
    }

    public void print() {
        for (int i = 0; i < currentLength; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < currentLength; i++)
            sb.append(array[i]).append(' ');
        return sb.toString();
    }

    public void addValue(int val) {
        if(getRealLength() > currentLength)
            array[currentLength++] = val;
        else {
            int[] targetArray = new int[getRealLength() + DEFAULT_RESERVE];
            System.arraycopy(array, 0, targetArray, 0, currentLength);
            array = targetArray;
            array[currentLength++] = val;
        }
    }

    public void deleteValue(int byPosition) {
        if(byPosition >= 0 && byPosition < currentLength) {
            for (int i = byPosition; i < currentLength - 1; i++)
                array[i] = array[i + 1];
            currentLength--;
        }
    }

    public int findPositionOf(int value) {
        if(!isSorted) {
            for (int i = 0; i != currentLength; i++)
                if (array[i] == value)
                    return i;
            return -1;
        } else
            return findBin(value, 0, currentLength);
    }

    private int findBin(int value, int l, int r) {
        int m = (l + r) / 2;
        if(array[m] == value)
            return m;
        else if(l == r)
            return -1;
        else {
            int tempRes = findBin(value, l, m);
            if(tempRes != -1)
                return tempRes;
            else
                return findBin(value, m + 1, r);
        }
    }

    public void sort(SortType bySortType, boolean withTimer, boolean andPrint) {
        long startTime = System.currentTimeMillis();
        switch(bySortType) {
            case BUBBLE:
                sortBubble();
                break;
            case SELECT:
                sortSelect();
                break;
            case INSERT:
                sortInsert();
                break;
            case QUICK:
                sortQuick();
                break;
            case MERGE:
                sortMerge();
                break;
        }
        isSorted = true;
        if(withTimer)
            System.out.println(System.currentTimeMillis() - startTime);
        if(andPrint)
            print();
    }

    private void sortBubble() {
        for(int last = currentLength - 2; last >= 0; last --)
            for(int i = 0; i <= last; i++) {
                if(array[i] > array[i+1])
                    swapValues(i, i+1);
        }
    }

    private void sortSelect() {
        for (int startPos = 0; startPos < currentLength - 1; startPos++) {
            int minValPos = startPos;
            for (int i = startPos + 1; i < currentLength; i++)
                if(array[i] < array[minValPos])
                    minValPos = i;
            swapValues(minValPos, startPos);
        }
    }

    private void sortInsert() {
        for (int startPos = 1; startPos < currentLength; startPos++) {
            int currentPos = startPos;
            while(currentPos != 0 && array[currentPos] < array[currentPos-1]) {
                swapValues(currentPos, currentPos - 1);
                currentPos--;
            }
        }
    }

    private void sortQuick() {
        sortQuick(0, currentLength - 1);
    }

    private void sortQuick(int l, int r) {
        if(l >= r)
            return;
        else if(l + 1 == r) {
            if(array[l] > array[r])
                swapValues(l, r);
            return;
        }
        else {
            int m = random.nextInt(r - l + 1) + l;
            int mVal = array[m];
            int originL = l, originR = r;
            while(true) {
                while(array[l] < mVal && l != r)
                    l++;
                while(array[r] >= mVal && r != l)
                    r--;
                if(l == r)
                    break;
                else
                    swapValues(l, r);
            }
            sortQuick(originL, l - 1);
            r = originR;
            while(true) {
                while(array[l] == mVal && l != r)
                    l++;
                while(array[r] > mVal && r != l)
                    r--;
                if(l == r)
                    break;
                else
                    swapValues(l, r);
            }
            sortQuick(r, originR);
        }
    }

    private void sortMerge() {
        mergeSortArray = new int[currentLength];
        sortMerge(0, currentLength - 1);
    }

    private void sortMerge(int l, int r) {
        if(l == r)
            return;
        else if(l + 1 == r) {
            if(array[l] > array[r])
                swapValues(l, r);
        }
        else {
            int m = (l + r) / 2;
            sortMerge(l, m);
            sortMerge(m + 1, r);
            for(int i = l, j = m + 1, mergeArrayIndex = l; mergeArrayIndex != r + 1;)
                if(i > m)
                    mergeSortArray[mergeArrayIndex++] = array[j++];
                else if(j > r)
                    mergeSortArray[mergeArrayIndex++] = array[i++];
                else if(array[i] < array[j])
                    mergeSortArray[mergeArrayIndex++] = array[i++];
                else
                    mergeSortArray[mergeArrayIndex++] = array[j++];
            for(int i = l; i <= r; i++)
                array[i] = mergeSortArray[i];
        }
    }

    private void swapValues(int source, int targer) {
        int tempVal = array[targer];
        array[targer] = array [source];
        array[source] = tempVal;
    }

}


package ru.syskaev.edu.geekbrains.java.cs.lesson2;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        startCli();
    }

    private static void startCli() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nBASE MODE\n")
                    .append("Choose:\n")
                    .append("1 Stack mode\n")
                    .append("2 Queue mode\n")
                    .append("3 Deque mode\n")
                    .append("4 Priority queue mode\n")
                    .append("5 Second task implementation\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        startStackMode();
                        break;
                    case 2:
                        startQueueMode();
                        break;
                    case 3:
                        startDequeMode();
                        break;
                    case 4:
                        startPriorityQueueMode();
                        break;
                    case 5:
                        doSecondTask();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static void startStackMode() {
        System.out.println("\nSTACK MODE");
        int size = getIntInput("Choose size <= 10", 2, 10);
        Stack stack = new Stack(size);
        System.out.println("Stack created");
        while(true) {
            try {
                System.out.println(new StringBuilder()
                        .append("\nSTACK MODE\n")
                        .append("Choose:\n")
                        .append("1 Push\n")
                        .append("2 Pop\n")
                        .append("3 Print\n")
                        .append("0 Exit"));
                int choice;
                if (scanner.hasNextInt()) {
                    choice = scanner.nextInt();
                    switch (choice) {
                        case 1:
                            stack.push(getIntInput("Enter value", Integer.MIN_VALUE, Integer.MAX_VALUE));
                            System.out.println("Pushed");
                            break;
                        case 2:
                            System.out.println(stack.pop());
                            break;
                        case 3:
                            stack.print();
                            break;
                        case 0:
                            return;
                        default:
                            System.out.println("Invalid input");
                    }
                } else {
                    scanner.next();
                    System.out.println("Invalid input");
                }
            } catch (CustomException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void startQueueMode() {
        System.out.println("\nQUEUE MODE");
        int size = getIntInput("Choose size <= 10", 2, 10);
        Queue queue = new Queue(size);
        System.out.println("Queue created");
        while(true) {
            try {
                System.out.println(new StringBuilder()
                        .append("\nQUEUE MODE\n")
                        .append("Choose:\n")
                        .append("1 Push\n")
                        .append("2 Pop\n")
                        .append("3 Print\n")
                        .append("0 Exit"));
                int choice;
                if (scanner.hasNextInt()) {
                    choice = scanner.nextInt();
                    switch (choice) {
                        case 1:
                            queue.push(getIntInput("Enter value", Integer.MIN_VALUE, Integer.MAX_VALUE));
                            System.out.println("Pushed");
                            break;
                        case 2:
                            System.out.println(queue.pop());
                            break;
                        case 3:
                            queue.print();
                            break;
                        case 0:
                            return;
                        default:
                            System.out.println("Invalid input");
                    }
                } else {
                    scanner.next();
                    System.out.println("Invalid input");
                }
            } catch (CustomException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void startDequeMode() {
        System.out.println("\nDEQUE MODE");
        int size = getIntInput("Choose size <= 10", 2, 10);
        Deque deque = new Deque(size);
        System.out.println("Deque created");
        while(true) {
            try {
                System.out.println(new StringBuilder()
                        .append("\nDEQUE MODE\n")
                        .append("Choose:\n")
                        .append("1 PushFront\n")
                        .append("2 PopFront\n")
                        .append("3 PushBack\n")
                        .append("4 PopBack\n")
                        .append("5 Print\n")
                        .append("0 Exit"));
                int choice;
                if (scanner.hasNextInt()) {
                    choice = scanner.nextInt();
                    switch (choice) {
                        case 1:
                            deque.push(getIntInput("Enter value", Integer.MIN_VALUE, Integer.MAX_VALUE));
                            System.out.println("Pushed");
                            break;
                        case 2:
                            System.out.println(deque.pop());
                            break;
                        case 3:
                            deque.pushBack(getIntInput("Enter value", Integer.MIN_VALUE, Integer.MAX_VALUE));
                            System.out.println("Pushed");
                            break;
                        case 4:
                            System.out.println(deque.popBack());
                            break;
                        case 5:
                            deque.print();
                            break;
                        case 0:
                            return;
                        default:
                            System.out.println("Invalid input");
                    }
                } else {
                    scanner.next();
                    System.out.println("Invalid input");
                }
            } catch (CustomException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void startPriorityQueueMode() {
        System.out.println("\nPRIORITY QUEUE MODE (value = priority)");
        int size = getIntInput("Choose size <= 10", 2, 10);
        PriorityQueue queue = new PriorityQueue(size);
        System.out.println("Queue created");
        while(true) {
            try {
                System.out.println(new StringBuilder()
                        .append("\nPRIORITY QUEUE MODE (value = priority)\n")
                        .append("Choose:\n")
                        .append("1 Push\n")
                        .append("2 Pop\n")
                        .append("3 Print\n")
                        .append("0 Exit"));
                int choice;
                if (scanner.hasNextInt()) {
                    choice = scanner.nextInt();
                    switch (choice) {
                        case 1:
                            queue.push(getIntInput("Enter value", Integer.MIN_VALUE, Integer.MAX_VALUE));
                            System.out.println("Pushed");
                            break;
                        case 2:
                            System.out.println(queue.pop());
                            break;
                        case 3:
                            queue.print();
                            break;
                        case 0:
                            return;
                        default:
                            System.out.println("Invalid input");
                    }
                } else {
                    scanner.next();
                    System.out.println("Invalid input");
                }
            } catch (CustomException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static int getIntInput(String msg, int l, int r) {
        while(true) {
            System.out.println(msg);
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                if(choice <= r && choice >= l)
                    return choice;
                else
                    System.out.println("Invalid input");
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static void doSecondTask() {
        System.out.println("\nSECOND TASK");
        System.out.println("Enter any string... (0 for exit)");
        String str;
        while (scanner.hasNext()) {
            str = scanner.next();
            if(str.equals("0"))
                break;
            else {
                for (int i = str.length() - 1; i >= 0; i--)
                    System.out.print(str.charAt(i));
                System.out.println();
            }
        }
    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson5;

import java.util.ArrayList;
import java.util.List;

public class Trees {

    private List<Tree> trees;

    public int getListSize() {return trees.size();}

    public Trees(int number) {
        trees = new ArrayList<>(number);
        for(int i = 0; i != number; i++) {
            Tree tree = new Tree();
            while(tree.addRandomVal());
            trees.add(tree);
        }
        System.out.println(number + " trees created");
    }

    public void showTree(int index) {
        System.out.println("Tree #" + index);
        trees.get(index - 1).displayTree();
    }

    public void showTrees() {
        for (int i = 0, lim = trees.size(); i < lim; i++) {
            System.out.println("Tree #" + (i + 1));
            trees.get(i).displayTree();
        }
    }

    public void checkBalance() {
        int counter = 0;
        for (int i = 0, lim = trees.size(); i < lim; i++) {
            boolean isBalanced = trees.get(i).checkBalance();
            if(isBalanced) {
                System.out.println("Tree #" + (i + 1) + " : balanced");
                counter++;
            }
        }
        if(counter != 0)
            System.out.println("Other trees unbalanced");
        else
            System.out.println("All trees unbalanced");
    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson2;

public class Queue extends BaseStructure {

    protected int front = 0;
    protected int back = 1;

    public Queue(int size) {
        super(size);
    }

    public void push(int val) throws CustomException {
        if(!isFull()) {
            back = prevIndex(back);
            baseArray[back] = val;
            currentSize++;
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is full");
    }

    public int pop() throws CustomException {
        if(!isEmpty()) {
            int tempVal = baseArray[front];
            front = prevIndex(front);
            currentSize--;
            return tempVal;
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is empty");
    }

    @Override
    public void print() throws CustomException {
        if(!isEmpty()) {
            for (int i = back; i != front; i = nextIndex(i))
                System.out.print(baseArray[i] + " ");
            System.out.print(baseArray[front] + " ");
            System.out.println();
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is empty");
    }

}

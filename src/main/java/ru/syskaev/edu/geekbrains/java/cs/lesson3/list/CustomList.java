package ru.syskaev.edu.geekbrains.java.cs.lesson3.list;

public class CustomList<T> {

    private Node<T> head = null;
    private Node<T> rear = null;

    public Node<T> getHead() { return head; }
    public void setHead(Node<T> head) { this.head = head; }
    public Node<T> getRear() { return rear; }
    public void setRear(Node<T> rear) { this.rear = rear; }

    public boolean isEmpty() {return head == null; }

}

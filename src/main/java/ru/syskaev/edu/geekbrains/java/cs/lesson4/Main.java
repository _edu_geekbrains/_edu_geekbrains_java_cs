package ru.syskaev.edu.geekbrains.java.cs.lesson4;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        startCli();
    }

    private static void startCli() {
        while(true) {
            System.out.println(new StringBuilder()
                    .append("\nBASE MODE\n")
                    .append("Choose:\n")
                    .append("1 Exponentiation\n")
                    .append("2 Rucksack problem\n")
                    .append("0 Exit"));
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                switch(choice) {
                    case 1:
                        System.out.println();
                        int base = getIntInput("Choose integer base >= 0 and <= 1000000", 0 , 1000000);
                        int exponent = getIntInput("Choose integer exponent >= 1 and <= 1000000", 1 , 1000000);
                        calcExponentiationAndPrint(base, exponent);
                        break;
                    case 2:
                        System.out.println();
                        int rucksackSize = getIntInput("Choose rucksack size >= 10 and <= 100000", 10 , 100000);
                        int itemsNumber = getIntInput("Choose number of items < 100", 1, 100);
                        Rucksack rucksack = new Rucksack(rucksackSize, itemsNumber);
                        rucksack.solveRucksackTask();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println("Invalid input");
                }
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static int getIntInput(String msg, int l, int r) {
        while(true) {
            System.out.println(msg);
            int choice;
            if(scanner.hasNextInt()) {
                choice = scanner.nextInt();
                if(choice <= r && choice >= l)
                    return choice;
                else
                    System.out.println("Invalid input");
            }
            else {
                scanner.next();
                System.out.println("Invalid input");
            }
        }
    }

    private static void calcExponentiationAndPrint(int base, int exponent) {
        //long time = System.currentTimeMillis();
        //System.out.println(BigInteger.valueOf(base).pow(exponent));
        //System.out.println("Standard method: " + (System.currentTimeMillis() - time) + "ms");
        //time = System.currentTimeMillis();
        System.out.println(calcExponentiation(BigInteger.valueOf(base), exponent));
        //System.out.println("Custom method: " + (System.currentTimeMillis() - time) + "ms");
    }

    private static BigInteger calcExponentiation(BigInteger base, int exponent) {
        if(exponent == 1)
            return base;
        else if(exponent % 2 == 0) {
            BigInteger temp = calcExponentiation(base, exponent / 2);
            return temp.multiply(temp);
        } else
            return calcExponentiation(base, exponent - 1).multiply(base);
    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson3;

import ru.syskaev.edu.geekbrains.java.cs.lesson3.list.CustomList;
import ru.syskaev.edu.geekbrains.java.cs.lesson3.list.CustomListIterator;
import ru.syskaev.edu.geekbrains.java.cs.lesson3.list.Node;

public class Main {

    private static CustomList<Person> list = new CustomList<>();
    private static CustomListIterator itr = new CustomListIterator(list);

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
    }

    private static void test1() {
        System.out.println("\ntest1");
        try {
            itr.printAll();
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test2() {
        System.out.println("\ntest2");
        try {
            itr.insertAfter(new Node<>(new Person()));
            itr.insertAfter(new Node<>(new Person()));
            itr.insertAfter(new Node<>(new Person()));
            itr.insertAfter(new Node<>(new Person()));
            itr.printAll();
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test3() {
        System.out.println("\ntest3");
        try {
            itr.reset();
            itr.next();
            itr.next();
            itr.insertAfter(new Node<>(new Person()));
            itr.insertBefore(new Node<>(new Person()));
            itr.printAll();
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test4() {
        System.out.println("\ntest4");
        try {
            itr.reset();
            itr.print();
            itr.resetToEnd();
            itr.print();
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test5() {
        System.out.println("\ntest5");
        try {
            itr.reset();
            itr.next();
            itr.next();
            while(true) {
                itr.printAll();
                itr.remove();
                System.out.println(".");
            }
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test6() {
        System.out.println("\ntest6");
        try {
            itr.insertBefore(new Node<>(new Person()));
            itr.insertBefore(new Node<>(new Person()));
            itr.printAll();
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    private static void test7() {
        System.out.println("\ntest7");
        try {
            itr.reset();
            Person person = (Person)itr.getCurrent();
            System.out.println(person.getName());
            itr.next();
            person = (Person)itr.getCurrent();
            System.out.println(person.getName());
        } catch (CustomException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

}

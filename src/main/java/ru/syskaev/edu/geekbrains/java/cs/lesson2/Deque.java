package ru.syskaev.edu.geekbrains.java.cs.lesson2;

public class Deque extends Stack {

    private int back = 1;

    public Deque(int size) {
        super(size);
    }

    protected void pushBack(int val) throws CustomException {
        if(!isFull()) {
            back = prevIndex(back);
            baseArray[back] = val;
            currentSize++;
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is full");
    }

    protected int popBack() throws CustomException {
        if(!isEmpty()) {
            int tempVal = baseArray[back];
            back = nextIndex(back);
            currentSize--;
            return tempVal;
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is empty");
    }

    @Override
    public void print() throws CustomException {
        if(!isEmpty()) {
            for (int i = back; i != front; i = nextIndex(i))
                System.out.print(baseArray[i] + " ");
            System.out.print(baseArray[front] + " ");
            System.out.println();
        }
        else
            throw new CustomException(this.getClass().getSimpleName() + " is empty");
    }

}

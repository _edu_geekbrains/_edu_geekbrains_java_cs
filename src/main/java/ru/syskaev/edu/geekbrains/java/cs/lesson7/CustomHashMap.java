package ru.syskaev.edu.geekbrains.java.cs.lesson7;

import ru.syskaev.edu.geekbrains.java.cs.lesson3.Person;

import java.util.Random;

public class CustomHashMap {

    private static final double MAX_CAPACITY_FACTOR = 0.75;
    private static final int MAX_SIZE_FOR_DISPLAY = 1000;

    private Item[] array;
    private Item[] reservArray = null;
    private int arraySize;
    private int arrayCapacity = 0;
    private Random random = new Random();

    public CustomHashMap(int size) {
        while(!isPrime(size))
            size++;
        array = new Item[size];
        arraySize = size;
        System.out.println("HashMap created, size = " + arraySize);
    }

    public void showAll() {
        if(arraySize > MAX_SIZE_FOR_DISPLAY)
            System.out.println("Too many entries");
        else
            for(int i = 0; i != arraySize; i++)
                if(array[i] == null || array[i].isNoItem())
                    System.out.println("void");
                else
                    System.out.println("Key = " + array[i].key + ", " + array[i].person);
    }

    public void insert() {
        insert(random.nextInt(Integer.MAX_VALUE / 2 + 1), null);
    }

    public void insert(int key, Person person) {
        if(getCurrentCapacityFactor() > MAX_CAPACITY_FACTOR)
            resize();
        int hash = keyHash(key) % arraySize;
        int index = hash;
        int step = stepHash(key) % arraySize;
        if(step == 0) step = 1;
        while(array[index] != null && !array[index].isNoItem())
            index = (index + step) % arraySize;
        if(person == null)
            person = new Person();
        array[index] = new Item(key, person);
        arrayCapacity++;
    }

    public Person find(int key) {
        int hash = keyHash(key) % arraySize;
        int index = hash;
        int step = stepHash(key) % arraySize;
        if (step == 0) step = 1;
        int counter = arraySize;
        while (counter-- != 0) {
            if (array[index] == null || array[index].isNoItem())
                return null;
            if(key == array[index].key)
                return array[index].person;
            index = (index + step) % arraySize;
        }
        return null;
    }

    public boolean delete(int key) {
        int hash = keyHash(key) % arraySize;
        int index = hash;
        int step = stepHash(key) % arraySize;
        if (step == 0) step = 1;
        int counter = arraySize;
        while (counter-- != 0) {
            if (array[index] == null || array[index].isNoItem())
                return false;
            if(key == array[index].key) {
                array[index].clearItem();
                arrayCapacity--;
                return true;
            }
            index = (index + step) % arraySize;
        }
        return false;
    }

    private double getCurrentCapacityFactor() {
        return (double)arrayCapacity / arraySize;
    }

    public void checkCapacity() {
        System.out.println("Array capacity = " + arrayCapacity);
        System.out.println(String.format("Capacity factor = %.3f", getCurrentCapacityFactor()));
    }

    private void resize() {
        System.out.println("Capacity factor is critical");
        checkCapacity();
        System.out.println("Start resize");
        reservArray = array;
        int oldSize = arraySize;
        int size = arraySize * 2;
        while(!isPrime(size))
            size++;
        array = new Item[size];
        arraySize = size;
        arrayCapacity = 0;
        System.out.println("Resized");
        transfer(oldSize);
    }

    private void transfer(int size) {
        System.out.println("Start transfer");
        for(int i = 0; i != size; i++)
            if(reservArray[i] == null || reservArray[i].isNoItem())
                continue;
            else
                insert(reservArray[i].key, reservArray[i].person);
        System.out.println("Transferred");
        checkCapacity();
        System.out.println();
    }

    private boolean isPrime(int a) {
        if(a == 1 || a % 2 == 0)
            return false;
        if(a == 2)
            return true;
        else {
            for(int i = 3; i * i < a; i += 2) {
               if(a % i == 0)
                   return false;
            }
        }
        return true;
    }

    private int keyHash(int key) {
        key ^= (key >>> 20) ^ (key >>> 12);
        return key ^ (key >>> 7) ^ (key >>> 4);
    }

    private int stepHash(long key) { //rot13
        int hash = 0;
        String str = String.valueOf(key * (key + 3));
        for(int i =0, lim = str.length(); i != lim; i++) {
            hash += str.charAt(i);
            hash -= (hash << 13) | (hash >> 19);
        }
        if(hash == 0)
            return stepHash(key + 1);
        return Math.abs(hash);
    }

    private class Item {

        private int key;
        private Person person;

        private Item(int key, Person person) {
            this.key = key;
            this.person = person;
        }

        private void clearItem() {
            key =-1;
            person = null;
        }

        private boolean isNoItem() {
            return (person == null);
        }

    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson3.list;

import ru.syskaev.edu.geekbrains.java.cs.lesson3.CustomException;

public class CustomListIterator {

    private CustomList list;
    private Node current;

    public CustomListIterator(CustomList list) {
        this.list = list;
        current = list.getHead();
    }

    public void reset() {
        current = list.getHead();
    }

    public void resetToEnd() {
        current = list.getRear();
    }

    private boolean hasNext() {
        return current.getNext() != null;
    }

    private boolean hasPrev() {
        return current.getPrev() != null;
    }

    public void next() throws CustomException {
        if(hasNext())
            current = current.getNext();
        else
            throw new CustomException("next() is impossible");
    }

    public void prev() throws CustomException {
        if(hasPrev())
            current = current.getPrev();
        else
            throw new CustomException("prev() is impossible");
    }

    boolean atEnd() {
        return !(hasNext());
    }

    boolean atStart() {
        return !(hasPrev());
    }

    public void insertAfter(Node node) {
        if(list.isEmpty())
            insertToEmptyList(node);
        else if(hasNext()){
            node.setNext(current.getNext());
            node.setPrev(current);
            current.setNext(node);
            node.getNext().setPrev(node);
        }
        else {
            current.setNext(node);
            node.setPrev(current);
            list.setRear(node);
        }
    }

    public void insertBefore(Node node) {
        if(list.isEmpty())
            insertToEmptyList(node);
        else if(hasNext()){
            node.setNext(current);
            node.setPrev(current.getPrev());
            current.setPrev(node);
            node.getPrev().setNext(node);
        }
        else {
            current.setPrev(node);
            node.setNext(current);
            list.setHead(node);
        }
    }

    private void insertToEmptyList(Node node) {
        current = node;
        list.setHead(node);
        list.setRear(node);
    }

    public void remove() throws CustomException {
        if(current == null)
            throw new CustomException("remove() is impossible");
        else if(hasNext() && hasPrev()) {
            current.getPrev().setNext(current.getNext());
            current.getNext().setPrev(current.getPrev());
            current = current.getPrev();
        } else if(hasNext()) {
            current.getNext().setPrev(null);
            current = current.getNext();
            list.setHead(current);
        } else if(hasPrev()) {
            current.getPrev().setNext(null);
            current = current.getPrev();
            list.setRear(current);
        } else {
            current = null;
            list.setHead(null);
            list.setRear(null);
        }
    }

    public Object getCurrent() {
        return current.getData();
    }

    public void print() throws CustomException {
        if(current != null)
            current.print();
        else
            throw new CustomException("current is null");
    }

    public void printAll() throws CustomException {
        if(list.isEmpty())
            throw new CustomException("list is empty");
        reset();
        while(true) {
            current.print();
            if(atEnd())
                break;
            else
                next();
        }
    }

}

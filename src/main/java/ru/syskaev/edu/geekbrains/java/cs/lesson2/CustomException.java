package ru.syskaev.edu.geekbrains.java.cs.lesson2;

public class CustomException extends Exception {

    public CustomException(String msg) {
        super(msg);
    }

}

package ru.syskaev.edu.geekbrains.java.cs.lesson6;

import ru.syskaev.edu.geekbrains.java.cs.lesson6.UI.MainWindow;

public class Main {

    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
        mainWindow.setVisible(true);
    }

}
